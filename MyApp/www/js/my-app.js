(function (global) {
    "use strict";
    function onDeviceReady () {
        document.addEventListener("online", onOnline, false);
        document.addEventListener("resume", onResume, false);
    }
 
    function onOnline () {

    }
 
    function onResume () {
        
    }
    document.addEventListener("deviceready", onDeviceReady, false);
})(window);

// Initialize your app
var myApp = new Framework7({
    animatePages: true,
    template7Pages: true,
    template7Data: {}
});

// Export selectors engine
var $$ = Dom7;
// Add views
var view1 = myApp.addView('#view-1', {

});
var view2 = myApp.addView('#view-2', {

});
var view3 = myApp.addView('#view-3', {

});
var view4 = myApp.addView('#view-4', {

});